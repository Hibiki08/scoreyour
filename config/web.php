<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'name' => 'ScoreYour.Work',
    'language' => 'rus-RUS',
    'timezone' => 'UTC',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'tSEEmqhqsLM2zF6ffKGti-pjav9vmHwA',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'report@scoreyour.work',
                'password' => '234mbmergjiokbmnb3453400',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'site/catcher.js' => 'site/get-catcher',
                '<action:(request-password-reset|reset-password|confirm-email)>' => 'site/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<params:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller>' => '<controller>/index',
            ],
        ],
        'geoip' => [
            'class' => 'lysenkobv\GeoIP\GeoIP'
        ],
        'walletone'=>[
            'class'=>'bookin\walletone\WalletOne',
            'secretKey' => '7c5b577b4b7838643843625959685f4350525a7951503051466171',
            'signatureMethod'=>'sha1',
            'buttonLabel'=>'Оплатить',
            'walletOptions'=>[
                'WMI_MERCHANT_ID'=>'189533066007',
                'WMI_CURRENCY_ID'=>'840',
                'WMI_SUCCESS_URL'=>['site/payment-success'],
                'WMI_FAIL_URL'=>['site/payment-fail'],
            ]
        ],
        'amocrm' => [
            'class' => 'app\components\amoCrm',

            // Для хранения ID полей можно воспользоваться хелпером
//            'fields' => [
//                'StatusId' => 10525225,
//                'ResponsibleUserId' => 697344,
//            ],
        ],
    ],
    'params' => $params,
    'defaultRoute' => 'stat',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
