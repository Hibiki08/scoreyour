<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modals".
 *
 * @property integer $id
 * @property string $step_0
 * @property string $step_1_1
 * @property string $step_1_2
 * @property string $step_2_1
 * @property string $step_2_2
 */
class Modals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['step_0', 'step_1_1', 'step_1_2', 'step_2_1', 'step_2_2'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'step_0' => 'Step 0',
            'step_1_1' => 'Step 1 1',
            'step_1_2' => 'Step 1 2',
            'step_2_1' => 'Step 2 1',
            'step_2_2' => 'Step 2 2',
        ];
    }
}
