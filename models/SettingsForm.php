<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Settings form
 */
class SettingsForm extends Model
{
    public $login;
    public $domain;
    public $company;
    public $current_password;
    public $new_password;
    public $repeat_password;
    public $amocrm_api_key;
    public $amocrm_login;
    public $amocrm_domain;
    public $amocrm_responsible_id;
    public $_user;

    const SCENARIO_RESET_PASS = 'reset_pass';
    const SCENARIO_COMPANY = 'company';
    const SCENARIO_AMOCRM = 'amocrm';


    public function init() {
        $this->_user = Users::findOne(['id' => Yii::$app->user->getId()]);
    }

    public function rules()
    {
        return [
            ['login', 'trim'],
            [['current_password', 'new_password', 'repeat_password', 'domain', 'amocrm_api_key', 'amocrm_login', 'amocrm_domain'], 'required'],
            ['login', 'email'],
            [['login', 'domain', 'company', 'amocrm_api_key', 'amocrm_login', 'amocrm_domain'], 'string', 'max' => 100],
            ['login', 'unique', 'targetClass' => '\app\models\Users', 'message' => 'Этот адрес уже зарегистрирован.'],
            ['new_password', 'string', 'min' => 6],
            ['repeat_password', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Пароли не совпадают'],
            ['current_password', 'validatePassword'],
        ];
    }

    public function scenarios() {
        return [
            self::SCENARIO_RESET_PASS => ['login', 'current_password', 'new_password', 'repeat_password'],
            self::SCENARIO_COMPANY => ['domain', 'company'],
            self::SCENARIO_AMOCRM => ['amocrm_api_key', 'amocrm_login', 'amocrm_domain', 'amocrm_responsible_id'],
        ];
    }

    public function resetPassword() {

        
        if (!$this->validate()) {
            return null;
        }

        if (!empty($this->new_password)) {
            $this->_user->setPassword($this->new_password);
        }

        if ($this->_user->save()) {
            return $this->_user;
        }
        return null;
    }
    
    public function saveCompany() {
        if (!$this->validate()) {
            return null;
        }

        $this->_user->domain = $this->domain;
        $this->_user->company = $this->company;
        
        if ($this->_user->save()) {
            return $this->_user;
        }
        return null;
    }

    public function saveAmoCrmSettings($amocrmUsers = []) {
        if (!$this->validate()) {
            return null;
        }

        $this->_user->amocrm_api_key = $this->amocrm_api_key;
        $this->_user->amocrm_login = $this->amocrm_login;
        $this->_user->amocrm_domain = $this->amocrm_domain;
        $this->_user->amocrm_responsible_id = $this->amocrm_responsible_id ? $this->amocrm_responsible_id : null;

        if ($this->_user->save()) {
            return $this->_user;
        }
        return null;
    }

    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            if (!$this->_user || !$this->_user->validatePassword($this->current_password)) {
                $this->addError($attribute, 'Неверный пароль.');
            }
        }
    }

    public function attributeLabels() {
        return [
            'login' => 'Email',
            'domain' => 'Домен',
            'company' => 'Компания',
            'current_password' => 'Текущий пароль',
            'new_password' => 'Новый пароль',
            'repeat_password' => 'Повтор пароля',
            'amocrm_api_key' => 'API ключ',
            'amocrm_login' => 'Логин на сайте amoCRM',
            'amocrm_domain' => 'Персональный поддомен на сайте amoCRM',
            'amocrm_responsible_id' => 'Ответственный на сайте amoCRM',
        ];
    }
    
}