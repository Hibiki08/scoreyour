<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $login;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'email'],
            ['login', 'exist',
                'targetClass' => '\app\models\Users',
                'filter' => ['status' => 1],
                'message' => 'Пользователь с указанным почтовым адресом не найден.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user Users */
        $user = Users::findOne([
            'status' => 1,
            'login' => $this->login,
        ]);

        if (!$user) {
            return false;
        }

        if (!Users::validatePasswordResetToken($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom('inbox@activemedia.pro')
            ->setTo($this->login)
            ->setSubject('Восстановление пароля для ScoreYour.Work')
            ->send();
    }

}