<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * RefererAnswerSearch represents the model behind the search form about `app\models\RefererAnswers`.
 */
class RefererAnswerSearch extends RefererAnswers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'answer', 'domain_id'], 'integer'],
            [['url', 'ip', 'comment', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = (new Query())->from('referer_answers');

        $query->innerJoin('domains', 'domains.id = domain_id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'answer' => $this->answer,
        ]);

        $query->andWhere(['domains.user_id' => Yii::$app->user->id]);

        if (!empty($this->date)) {
            $query->andFilterWhere(['like', 'date', Yii::$app->formatter->asDate($this->date, 'yyyy-MM-dd')]);
        }
        
        $query->andFilterWhere(['=', 'domain_id', $this->url])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->orderBy(['date' => SORT_DESC]);

        return $dataProvider;
    }
}
