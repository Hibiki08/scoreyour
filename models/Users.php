<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $access_token
 * @property string $company
 * @property string $amocrm_api_key
 * @property string $amocrm_login
 * @property string $amocrm_domain
 * @property string $amocrm_responsible_id
 * @property string $domain
 * @property string $domain_key
 * @property string $status
 * @property string $notification_mail
 * @property string $subscription_date
 * @property string $subscription_status
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    const STATUS_ACTIVE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['id', 'status'], 'integer'],
            [['login', 'password', 'auth_key', 'access_token', 'company', 'notification_mail'], 'string', 'max' => 100],
            [['domain'], 'string', 'max' => 255],
            [['domain_key'], 'string', 'max' => 500],
            [['login'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'company' => 'Компания',
            'notification_mail' => 'Почтовый адрес',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        $user = Users::find()
            ->where([
                "id" => $id
            ])
            ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $userType = null) {

        $user = Users::find()
            ->where(["access_token" => $token])
            ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }

    /**
     * Finds user by login
     *
     * @param  string      $login
     * @param  bool      $new
     * @return static|null
     */
    public static function findByLogin($login, $new = true) {
        $user = Users::find()
            ->where([
                "login" => $login
            ])
            ->one();
        if (!count($user)) {
            return null;
        }
        return $new ? new static($user) : $user;
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::validatePasswordResetToken($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => 1,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    public function getNotificationMail() {
        return $this->notification_mail;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->auth_key === $authKey;
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function validatePasswordResetToken($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function setNotificationMail($mail) {
        $this->notification_mail = $mail;
    }

    /**
     * Generates authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generateDomainKey() {
        $this->domain_key = md5(time() . 'akOdv9');
    }

    public static function getDomainKey() {
        return self::find()
            ->select('domain_key')
            ->where(['id' => Yii::$app->user->id])
            ->one();
    }

}
