<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $login;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'email'],
            ['login', 'string', 'max' => 100],
            ['login', 'unique', 'targetClass' => '\app\models\Users', 'message' => 'Этот адрес уже зарегистрирован.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return Users|null the saved model or null if saving fails
     */
    public function signup() {

        if (!$this->validate()) {
            return null;
        }

        $subscriptionDate = new \DateTime(date('Y-m-d'));

        $user = new Users();
        $user->login = $user->notification_mail = $this->login;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateDomainKey();
        $user->subscription_date = $subscriptionDate->modify('+1 month')->format('Y-m-d');
        $user->subscription_status = 1;

        $send = Yii::$app->mailer->compose('emailConfirm', ['user' => $user])
            ->setFrom('report@scoreyour.work')
            ->setTo($this->login)
            ->setSubject('Подтверждение регистрации на ' . Yii::$app->name)
            ->send();

        if ($send) {
           if ($user->save()) {
               $lasId = $user->id;
               $userRole = Yii::$app->authManager->getRole('user');
               Yii::$app->authManager->assign($userRole, $lasId);
               $permit = Yii::$app->authManager->getPermission('userPermission');
               Yii::$app->authManager->assign($permit, $lasId);
               return $user;
           }
        }
        return null;
    }

    public function attributeLabels() {
        return [
            'login' => 'Email',
            'password' => 'Пароль',
        ];
    }

}