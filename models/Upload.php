<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Upload extends Model
{
    public $file;

    public function rules()
    {
        return [

            [['file'],'file', 'skipOnEmpty' => false, 'mimeTypes' => 'image/*'],

        ];
    }

    public function upload($id, $name)
    {
        if (!file_exists(Yii::getAlias("@app/web/resources/images/modal/$id"))) {
            mkdir(Yii::getAlias("@app/web/resources/images/modal/$id"), 0775, true);
        }

        $path = Yii::getAlias("@app/web/resources/images/modal/$id/$name.{$this->file->extension}");

        if ($this->file->saveAs($path)) {
            return "/resources/images/modal/$id/$name.{$this->file->extension}";
        } else {
            return false;
        }
    }
}