<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Modals;

/**
 * ModalSearch represents the model behind the search form about `app\models\Modals`.
 */
class ModalSearch extends Modals
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['step_0', 'step_1_1', 'step_1_2', 'step_2_1', 'step_2_2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Modals::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'step_0', $this->step_0])
            ->andFilterWhere(['like', 'step_1_1', $this->step_1_1])
            ->andFilterWhere(['like', 'step_1_2', $this->step_1_2])
            ->andFilterWhere(['like', 'step_2_1', $this->step_2_1])
            ->andFilterWhere(['like', 'step_2_2', $this->step_2_2]);

        return $dataProvider;
    }
}
