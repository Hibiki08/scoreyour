<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "domains".
 *
 * @property integer $id
 * @property string $host
 * @property string $name
 * @property integer $user_id
 */
class Domains extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'domains';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['host', 'name', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['host', 'name'], 'string', 'max' => 255],
        ];
    }

    public static function getDomainsByUser() {
        return self::find()
            ->select('id, host')
            ->where(['user_id' => Yii::$app->user->id])
            ->orderBy(['host' => SORT_ASC])
            ->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => 'Домен',
            'name' => 'Название',
            'user_id' => 'User ID',
        ];
    }
}
