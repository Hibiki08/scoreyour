<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "referer_answers".
 *
 * @property integer $id
 * @property string $url
 * @property string $ip
 * @property integer $answer
 * @property string $comment
 * @property string $date
 * @property integer $domain_id
 */
class RefererAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referer_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'ip', 'answer', 'domain_id'], 'required'],
            [['answer', 'domain_id'], 'integer'],
            [['date'], 'safe'],
            [['url'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 15],
            [['comment'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'ip' => 'Посетитель',
            'answer' => 'Ответ',
            'comment' => 'Комментарий',
            'date' => 'Дата',
            'domain_id' => 'Domain ID',
        ];
    }
}
