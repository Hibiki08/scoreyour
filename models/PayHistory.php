<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "modals".
 *
 * @property integer $id
 * @property string $description
 * @property string $pay_status
 * @property string $user_id
 * @property string $wallet_data
 * @property string $date
 */
class PayHistory extends ActiveRecord {

    public static function tableName() {
        return 'pay_history';
    }
    
    public static function getHistory($command = false) {
        $query = self::find()->where(['user_id' => Yii::$app->user->id]);
        
        if ($command) {
            return $query;
        }
        return $query->all();
    }

}
