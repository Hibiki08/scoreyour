<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\Users;


class SubChecker extends Widget {

    public function run() {
        $user = Users::findOne(['id' => Yii::$app->user->id]);
        
        return $this->render('sub_checker', [
            'subscription_status' => $user['subscription_status'],
            'subscription_date' => $user['subscription_date']
        ]);
    }
    
}