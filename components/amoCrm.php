<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\amocrm\Client;


class amoCrm extends Client {

    public $error = '';
    
    public function __construct($subdomain, $hash, $login) {
        parent::__construct();
        $this->subdomain = $subdomain;
        $this->hash = $hash;
        $this->login = $login;

        if (empty($this->subdomain) || empty($this->hash) || empty($this->login)) {
            $this->error = 'Не заполнены необходимые поля';
        }
    }

    public function getUsers() {
        $amocrmUsers = null;
        if (empty($this->error)) {
            try {
                $account = $this->account;
                $accountInfo = $account->apiCurrent();
                if (!empty($accountInfo['users'])) {
                    $users = $accountInfo['users'];
                    foreach ($users as $user) {
                        $amocrmUsers[$user['id']] = $user['login'];
                    }
                }
            } catch (\AmoCRM\Exception $e) {
                $this->error = sprintf('Error (%d): %s', $e->getCode(), $e->getMessage());
            }
        }
        return $amocrmUsers;
    }

}