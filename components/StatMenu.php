<?php

namespace app\components;

use yii\base\Widget;


class StatMenu extends Widget {

    public function run() {
        return $this->render('stat_menu');
    }

}