<?php

namespace app\components;

use yii\base\Widget;


class AccountMenu extends Widget {

    public function run() {
        return $this->render('account_menu');
    }

}