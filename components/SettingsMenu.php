<?php

namespace app\components;

use yii\base\Widget;


class SettingsMenu extends Widget {

    public function run() {
        return $this->render('settings_menu');
    }

}