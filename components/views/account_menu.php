<?php
use yii\helpers\Url;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
?>
<div class="sidebar col-lg-3">
    <nav class="navbar navbar-default sidebar" >
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $action == 'reset-password' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['settings/reset-password']); ?>"><span class="hidden-xs showopacity glyphicon glyphicon-user"></span>Аккаунт</a></li>
                    <li class="<?php echo $action == 'index' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/settings']); ?>"><span class="hidden-xs glyphicon glyphicon-home"></span>Компания</a></li>
                    <li class="<?php echo $action == 'pay-history' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['settings/pay-history']); ?>"><span class="hidden-xs glyphicon glyphicon-usd"></span>История платежей</a></li>
                    <li class="<?php echo $action == 'amo-crm' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['settings/amo-crm']); ?>"><span class="hidden-xs glyphicon glyphicon-random"></span>Интеграция amoCRM</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>