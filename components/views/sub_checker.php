<?php
use yii\helpers\Url;
?>

<?php if ($subscription_status) {?>
<blockquote id="pay-subscription">
    <p>Подписка истекает: <?php echo Yii::$app->formatter->asDate($subscription_date, 'd.MM.yyyy'); ?></p>
</blockquote>
<?php } else { ?>
    <blockquote id="pay-subscription" class="expired">
        <p>Срок подписки истёк. Вы можете её продлить <a href="<?php echo Url::to(['site/subscription']); ?>">здесь</a>.</p>
    </blockquote>
<?php } ?>
