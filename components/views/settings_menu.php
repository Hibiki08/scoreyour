<?php
use yii\helpers\Url;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
?>
<div class="sidebar col-lg-3">
    <nav class="navbar navbar-default sidebar" >
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $controller == 'modal' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/modal']); ?>"><i class="glyphicon glyphicon-picture"></i>Внешний вид</a></li>
                    <li class="<?php echo $controller == 'notification' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/notification']); ?>"><i class="glyphicon glyphicon-link"></i>Уведомления</a></li>
                    <li class="<?php echo $controller == 'domain' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/domain']); ?>"><i class="glyphicon glyphicon-globe"></i>Домены</a></li>
                    <li class="<?php echo $action == 'subscription' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['site/subscription']); ?>"><i class="glyphicon glyphicon-rub"></i>Подписка</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>