<?php
use yii\helpers\Url;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
?>
<div class="sidebar col-lg-3">
    <nav class="navbar navbar-default sidebar" >
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $action == 'index' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/stat']); ?>"><span class="hidden-xs glyphicon glyphicon-user"></span>Ответы</a></li>
                    <li class="<?php echo $action == 'domains' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['stat/domains']); ?>"><span class="hidden-xs glyphicon glyphicon-globe"></span>Сайты</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>