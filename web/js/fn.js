$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.catch-popup').resizable({
        stop: function (event, ui) {
            modalEdit();
        }
    });
    $('.popup-content')
        .draggable({
            containment: "parent",
            scroll: false,
            stop: function (event, ui) {
                modalEdit();
            }
        })
        .resizable({
            containment: "parent",
            stop: function (event, ui) {
                modalEdit();
            }
        });
    //$('.js-editable').popover();
    //$.fn.editable.defaults.mode = 'inline';
    $('.js-editable').editable().on('hidden', function () {
        modalEdit();
    });

} );
function modalEdit() {
    $('.spinner').css('opacity', 1);
    var data = {};
    var modal = $('#modals-save').data('id');

    $('.tab-pane').each(function () {
        var id = $(this).attr('id');
        data[id] = {};
        $('#' + id + ' .js-editable').each(function () {
            data[id][$(this).data('name')] = $(this).html();
        });


        var content = $(this).find('.popup-content');
        var popup = $(this).find('.catch-popup');

        $(this).addClass('active');
        data[id]['w-content'] = content.css('width');
        data[id]['h-content'] = content.css('height');
        data[id]['t-content'] = content.css('top');
        data[id]['l-content'] = content.css('left');
        data[id]['w-popup'] = popup.css('width');
        data[id]['h-popup'] = popup.css('height');
        data[id]['bg-popup'] = popup.css('background-image');
        if (!$(this).hasClass('in')) {
            $(this).removeClass('active');
        }
    });
    data._csrf = yii.getCsrfToken();

    $.ajax({
        type: 'POST',
        url: '/modal/update?id=' + modal,
        data: data,
        success: function(data){
            if (data != false) {
                $('.spinner').css('opacity', 0);
                return true;
            }
        }, error: function (data) {
            return false;
        }
    });
}

$(document).ready(function () {
    $('.js-modal-background').on('click', function (e) {
        $('#file-input').data('target', $(this).data('target') == 'this' ? $(this).parents('.tab-pane').attr('id') : 'all');
        /*e.preventDefault();
        var target = $(this).data('target') == 'this' ? $(this).parents('.tab-pane').attr('id') : 'all';
        var id = $('#modals-save').data('id');

        fileDialog({ accept: 'image/*' })
            .then(file => {
            const data = new FormData()
            data.append('Upload[file]', file[0])
            data.append('name', target)

            $.ajax({
                type: 'POST',
                url:"/modal/upload-image?id=" + id,
                data: data,
                processData: false,
                contentType: false,
                success: function(data){
                    if (data != false) {
                        if (target == 'all') {
                            $('.catch-popup').css('background-image', 'url(' + data + ')')
                        } else {
                            $('#' + target + ' .catch-popup').css('background-image', 'url(' + data + ')')
                        }
                    } else {
                        alert('Неудалось загрузить изображение.');
                    }
                }
            })
        })*/
    });

    $('#file-input').on('change', function () {
        if (this.files.length > 0) {
            $('.spinner').show();
            var id = $('#modals-save').data('id');
            var target = $(this).data('target');
            const data = new FormData();
            data.append(this.name, this.files[0]);
            data.append('name', target);

            $.ajax({
                type: 'POST',
                url:"/modal/upload-image?id=" + id,
                data: data,
                processData: false,
                contentType: false,
                success: function(data){
                    if (data != false) {
                        var now = new Date().getTime();
                        if (target == 'all') {
                            $('.catch-popup').css('background-image', 'url(' + data + '?r' + now + ')');
                            if(modalEdit()) {
                                $('.spinner').css('opacity', 0);
                            }
                        } else {
                            $('#' + target + ' .catch-popup').css('background-image', 'url(' + data + '?r' + now + ')');
                            if(modalEdit()) {
                                $('.spinner').css('opacity', 0);
                            }
                        }

                    } else {
                        alert('Неудалось загрузить изображение.');
                        // $('.spinner').hide();
                    }
                }
            });
        }
    });
});