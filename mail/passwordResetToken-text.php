<?php

/* @var $user app\models\Users */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'login', 'token' => $user->password_reset_token]);
?>

    Здравствуйте, для вашего аккаунта на ScoreYour.Work был запрошен сброс пароля.
    Для сброса пароля пройдите по ссылке:

<?= $resetLink ?>