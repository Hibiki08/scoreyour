<?php

use yii\helpers\Html;

/* @var $user app\models\Users */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

<div class="password-reset">
    <p>Здравствуйте, для вашего аккаунта на <?= Html::a('ScoreYour.Work', Yii::$app->urlManager->createAbsoluteUrl('/')) ?> был запрошен сброс пароля.</p>
    <p>Для сброса пароля пройдите по ссылке:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>