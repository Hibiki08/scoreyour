<?php
use yii\helpers\Html;
use yii\grid\GridView;

?>

<p>Статистика за прошедшую неделю:</p>

<p>Лидер переходов: <??></p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'summary'=>'',
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'host:url:Домен',
        'all:text:Всего проголосовавших',
        [
            'attribute' => 'Результат (+/−)',
            'format' => 'raw',
            'value' => function ($model) {
                return "{$model['yes']}/" . ($model['all'] - $model['yes']);
            },
        ],
    ],
]); ?>

<?= Html::a('Посмотреть все отзывы на ScoreYour.Work', Yii::$app->urlManager->createAbsoluteUrl('/stat')) ?>