<?php
use yii\helpers\Html;

?>

<p>Добавлен новый комментарий:</p>

<p>Домен: <?= Html::a(Html::encode($domain->host), $domain->host) ?></p>
<p>IP: <?= $answer->ip ?></p>
<p>Ответ: <?= $answer->answer ? 'Да' : 'Нет' ?></p>
<p>Комментарий: <?= $answer->comment ?></p>

<?= Html::a('Посмотреть отзывы на ScoreYour.Work', Yii::$app->urlManager->createAbsoluteUrl('/stat')) ?>