<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\Users */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm-email', 'email' => $user->login, 'token' => $user->auth_key]);
?>

<p>Здравствуйте, ваш почтовый адрес был использован для регистрации на <?= Html::a('ScoreYour.Work', Yii::$app->urlManager->createAbsoluteUrl('/')) ?>!</p>
<br>
<p>Для подтверждения адреса пройдите по ссылке:</p>
<?= Html::a(Html::encode($confirmLink), $confirmLink) ?>
<br>
<p>Если Вы не регистрировались на нашем сайте, то проигнорируйте это письмо.</p>