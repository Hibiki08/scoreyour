<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\RequestSearch;
use app\models\Users;
use yii\helpers\Url;


class SubCheckerController extends Controller {

    public function actionStart() {
        $res = true;
        $currentData = date('Y-m-d');
        $users = Users::find()->where(['and', ['status' => 1], ['<', 'subscription_date', $currentData]])->all();

        $expired = [];
        if (!empty($users)) {
            foreach ($users as $user) {
                $expired[] = $user['id'];
            }
            if (Users::updateAll(['subscription_status' => 0],  ['in', 'id', $expired]) === false) {
                $res = false;
            }
        }
        if ($res) {
            echo 'OK';
        }
    }

}