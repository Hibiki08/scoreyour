<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\SettingsMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DomainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Домены';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stat-wrapper row">
    <?php echo SettingsMenu::widget(); ?>
    <div class="domains-index col-lg-9">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <p>Всплывающее окно реагирует только на переходы с этих сайтов.</p>
        <p>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить сайт', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary'=>'',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'host',
                'name',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}'
                ],
            ],
        ]); ?>
    </div>
</div>

