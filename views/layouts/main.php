<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\SubChecker;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--
    TODO: Выпилить эту хрень, подключить метрику через https://github.com/hiqdev/yii2-yandex-metrika
-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45292806 = new Ya.Metrika({
                    id:45292806,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45292806" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="wrap">
    <?php
    $company = '';
    $user = Yii::$app->user->identity;
    if($user && !empty($user->company)) {
        $company = $user->company;
    }
    NavBar::begin([
        'brandLabel' => "ScoreYour.Work <small>&nbsp;$company</small><i class=\"glyphicon glyphicon-refresh spinner js-spinner\"></i>",
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->user->isGuest) {
        $items = [
            ['label' => '<i class="glyphicon glyphicon-stats"></i> Статистика',
                'url' => ['#'],
                'items' => [
                    ['label' => '<i class="glyphicon glyphicon-user"></i> Ответы', 'url' => ['/stat']],
                    ['label' => '<i class="glyphicon glyphicon-globe"></i> Сайты', 'url' => ['stat/domains']],
                ]
            ],
            ['label' => '<i class="glyphicon glyphicon-wrench"></i> Настройки',
                'url' => ['#'],
                'items' => [
                    ['label' => '<i class="glyphicon glyphicon-picture"></i> Внешний вид', 'url' => ['/modal/']],
                    ['label' => '<i class="glyphicon glyphicon-link"></i> Уведомления и интеграция', 'url' => ['/notification/']],
                    ['label' => '<i class="glyphicon glyphicon-globe"></i> Домены', 'url' => ['/domain/']],
                    ['label' => '<i class="glyphicon glyphicon-rub"></i> Подписка', 'url' => ['/site/subscription/']],
                ],
            ],
            ['label' => Yii::$app->user->identity->login,
                'url' => ['#'],
                'items' => [
                    ['label' => '<i class="showopacity glyphicon glyphicon-user"></i> Аккаунт', 'url' => ['settings/reset-password']],
                    ['label' => '<i class="glyphicon glyphicon-home"></i> Компания', 'url' => ['/settings']],
                    ['label' => '<i class="glyphicon glyphicon-usd"></i> История платежей', 'url' => ['settings/pay-history']],
                    ['label' => '<i class="glyphicon glyphicon-random"></i> Интеграция amoCRM', 'url' => ['/settings/amo-crm/']],
                    '<li class="divider"></li><li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выйти',
                        ['class' => 'btn btn-link']
                    )
                    . Html::endForm()
                    . '</li>'
                ]
            ],
        ];
    } else {
        $items = [
            ['label' => 'Войти', 'url' => ['/site/login']],
            ['label' => 'Зарегистрироваться', 'url' => ['/site/signup']],
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $items,
    ]);
    NavBar::end(); ?>

    <div class="container">
        <?php echo !Yii::$app->user->isGuest ? SubChecker::widget() : ''; ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ScoreYour.Work <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
    <?php if (Yii::$app->user->can('adminPermission')) { ?>
        <div class="footer-develop">Разработка сайта — «<a href="https://cdn.activemedia.pro" target="_blank">Тест</a>»</div>
    <?php } ?>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
