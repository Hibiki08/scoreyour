<?php

use app\components\AccountMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Аккаунт';
?>
<div class="profile-wrapper row">
    <?php echo AccountMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <div class="form">
            <?php $form = ActiveForm::begin([
                'id' => 'password_reset',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => '{label}{input}{error}',
                ],
            ]); ?>
            <fieldset class="form-group">
                <legend><?php echo $this->title; ?></legend>
                <?php if (Yii::$app->session->getFlash('success_reset')) {?>
                    <div class="alert alert-success fade in alert-dismissable">
                        <p class="success"><?php echo Yii::$app->session->getFlash('success_reset'); ?></p>
                    </div>
                <?php } ?>
                <?php echo $form->field($model, 'login', [
                ])->input('text', ['value' => $user->login, 'disabled' => 'disabled']); ?>
                <?php echo $form->field($model, 'current_password')->input('password'); ?>
                <?php echo $form->field($model, 'new_password')->input('password'); ?>
                <?php echo $form->field($model, 'repeat_password')->input('password'); ?>
            </fieldset>
            <button type="submit" class="btn">Изменить пароль</button>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
