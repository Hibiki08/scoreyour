<?php

use app\components\AccountMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Настройки компании';
?>
<div class="profile-wrapper row">
    <?php echo AccountMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <div class="form">
            <?php $form = ActiveForm::begin([
                'id' => 'pass-reset',
                'enableClientValidation' => false,
            ]); ?>
            <fieldset class="form-group">
                <legend><?php echo $this->title; ?></legend>
                <blockquote>
                    <p>Код для вставки на сайт:</p>
                    <code>&lt;script src="https://cdn.scoreyour.work/site/catcher.js?domain_key=<?php echo $domainKey['domain_key']; ?>"&gt;&lt;/script&gt;</code>
                </blockquote>
                <?php if (Yii::$app->session->getFlash('success_company')) {?>
                    <div class="alert alert-success fade in alert-dismissable">
                        <p class="success"><?php echo Yii::$app->session->getFlash('success_company'); ?></p>
                    </div>
                <?php } ?>
                <?php echo $form->field($model, 'company')->input('text', ['value' => $user->company]); ?>
                <?php echo $form->field($model, 'domain')->input('text', ['value' => $user->domain]); ?>
            </fieldset>
            <button type="submit" class="btn">Сохранить</button>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
