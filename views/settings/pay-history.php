<?php

use app\components\AccountMenu;
use yii\helpers\Url;
//use yii\bootstrap\ActiveForm;
use yii\grid\GridView;

$this->title = 'История платежей';
?>
<div class="profile-wrapper row">
    <?php echo AccountMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <legend><?php echo $this->title; ?></legend>
        <?php echo GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'summary'=>'',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                'id:text:ID',
                'description:text:Описание',
                [
                    'attribute' => 'date',
                    'label' => 'Дата',
                    'format' =>  ['datetime', 'dd.MM.Y HH:mm:ss']
                ],
                [
                    'label' => 'Статус оплаты',
                    'attribute' => 'pay_status',
                    'value' => function($data) {
                        $dataWallet = unserialize($data->wallet_data);
                        return $data->pay_status ? 'OK' : $dataWallet['WMI_ORDER_STATE'];
                    },
                    'format' => 'raw'
                ],
            ],
        ]); ?>
    </div>
</div>
