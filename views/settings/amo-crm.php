<?php

use app\components\AccountMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;

$this->title = 'Интеграция amoCRM';
?>
<div class="profile-wrapper row">
    <?php echo AccountMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <legend><?php echo $this->title; ?></legend>
        <div class="form">
            <?php $form = ActiveForm::begin([
                'id' => 'amocrm',
                'enableClientValidation' => false,
            ]); ?>
            <fieldset class="form-group">
                <?php if (Yii::$app->session->getFlash('success_amocrm')) {?>
                    <div class="alert alert-success fade in alert-dismissable">
                        <p class="success"><?php echo Yii::$app->session->getFlash('success_amocrm'); ?></p>
                    </div>
                <?php } ?>
                <?php echo $form->field($model, 'amocrm_api_key')->input('text', ['value' => $user->amocrm_api_key]); ?>
                <?php echo $form->field($model, 'amocrm_login')->input('text', ['value' => $user->amocrm_login]); ?>
                <?php echo $form->field($model, 'amocrm_domain')->input('text', ['value' => $user->amocrm_domain]); ?>
                <?php if (!empty($amocrmUsers)) { ?>
                <?php echo $form->field($model, 'amocrm_responsible_id')->dropDownList($amocrmUsers, ['value' => $user->amocrm_responsible_id]); ?>
                <?php } ?>
            </fieldset>
            <button type="submit" class="btn">Сохранить</button>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
