<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefererAnswers */

$this->title = 'Create Referer Answers';
$this->params['breadcrumbs'][] = ['label' => 'Referer Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="referer-answers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
