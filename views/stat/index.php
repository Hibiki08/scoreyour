<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\StatMenu;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RefererAnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $topReferer app\models\RefererAnswers */
/* @var $successReferer app\models\RefererAnswers */
/* @var $failReferer app\models\RefererAnswers */

$this->title = 'Ответы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stat-wrapper row">
    <?php echo StatMenu::widget(); ?>
    <div class="referer-answers-index col-lg-9">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php if ($dataProvider->getTotalCount() < 5) {?>
            <p>Мало данных</p>
        <?php } else { ?>
            <div class="col-md-4 bg-info">
                <h4>Лидер переходов</h4>
                <h2><?= $topReferer['all'] ?> <small><?= $topReferer['host'] ?></small><??></h2>
            </div>
            <div class="col-md-4 bg-success">
                <h4>Лучший рейтинг</h4>
                <h2><?= round($successReferer['stat']) ?>% <small><?= $successReferer['host'] ?></small></h2>
            </div>
            <div class="col-md-4 bg-danger">
                <h4>Проект-провал</h4>
                <h2><?= round($failReferer['stat']) ?>% <small><?= $failReferer['host'] ?></small></h2>
            </div>
        <?php } ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary'=>'',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                [
                    'label' => 'Дата',
                    'attribute' => 'date',
                    'filter' => DatePicker::widget([
                        'language' => 'ru',
                        'model' => $searchModel,
                        'attribute' => 'date',
                        'dateFormat' => 'dd.MM.yyyy'
                    ]),
                    'format' =>  'html'
                ],
                [
                    'label' => 'Url',
                    'attribute' => 'url',
                    'value' => function($data) {
                        return '<a href="' . $data['url'] . '" target="__blank">' . $data['url'] . '</a>';
                    },
                    'filter' => $urls,
                    'format' => 'raw'
                ],
                [
                    'label' => 'Посетитель',
                    'value' => function($data) {
                        $geoData = Yii::$app->geoip->ip($data['ip']);
                        return $data['ip'] . ' <span data-toggle="tooltip" data-placement="right" title="' . $geoData->country . ', ' . $geoData->city .'"><i class="glyphicon glyphicon-info-sign"></i></span>';
                    },
                    'format' => 'raw'
                ],
                [
                    'label' => 'Ответ',
                    'value' => function($data) {
                        return $data['answer'] ? 'Да' : 'Нет';
                    }
                ],
                'comment',
            ],
        ]); ?>
    </div>
</div>

