<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\StatMenu;

/* @var $statisticsDP yii\data\ActiveDataProvider */

$this->title = 'Сайты';
?>
<div class="stat-wrapper row">
    <?php echo StatMenu::widget(); ?>
    <div class="referer-answers-index col-lg-9">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $statisticsDP,
        'summary'=>'',
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'host:url:Домен',
            'all:text:Всего проголосовавших',
            [
                'label' => 'Результат (+/−)',
                'format' => 'raw',
                'value' => function ($model) {
                    $yesPrcnt = round($model['yes']/$model['all']*100);
                    //$result .= "% ({$model['yes']}/";
                    //$result .= $model['all'] - $model['yes'];
                    //$result .= ')';
                    //return $result;
                    return '<div class="bg-danger" style="display: block; min-height: 100%;"><span class="bg-success" style="display: inline-block; min-height: 100%; width:'.$yesPrcnt.'%">&nbsp;</span></div>';
                },
            ],
        ],
    ]); ?>
    </div>
</div>
