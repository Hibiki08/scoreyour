<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Modals */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modals-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'step_0')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'step_1_1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'step_1_2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'step_2_1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'step_2_2')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
