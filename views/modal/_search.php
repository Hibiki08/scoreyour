<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ModalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modals-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'step_0') ?>

    <?= $form->field($model, 'step_1_1') ?>

    <?= $form->field($model, 'step_1_2') ?>

    <?= $form->field($model, 'step_2_1') ?>

    <?php // echo $form->field($model, 'step_2_2') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
