<?php
use yii\bootstrap\Html;
use yii\grid\GridView;
use app\components\SettingsMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ModalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Внешний вид всплывающего окна';
$this->params['breadcrumbs'][] = $this->title;
$steps = [];
foreach ($model as $key => $value) {
    $value = @unserialize($value);
    if (!empty($value) && $value !== false) {
        $steps[$key] = $value;
    }
}
?>
<style>
    <?php foreach ($steps as $id => $step): ?>
        #<?= $id ?> .popup-content {
            <?php if (!empty($step['w-content'])): ?>
                width: <?= $step['w-content'] ?>;
            <? endif; ?>
            <?php if (!empty($step['h-content'])): ?>
                height: <?= $step['h-content'] ?>;
            <? endif; ?>
            <?php if (!empty($step['t-content'])): ?>
                top: <?= $step['t-content'] ?>;
            <? endif; ?>
            <?php if (!empty($step['l-content'])): ?>
                left: <?= $step['l-content'] ?>;
            <? endif; ?>
        }
        #<?= $id ?> .catch-popup {
            <?php if (!empty($step['w-popup'])): ?>
                width: <?= $step['w-popup'] ?>;
            <? endif; ?>
            <?php if (!empty($step['h-popup'])): ?>
                height: <?= $step['h-popup'] ?>;
            <? endif; ?>
            <?php if (!empty($step['bg-popup'])): ?>
                background-image: <?= $step['bg-popup'] ?>;
            <? endif; ?>
        }
    <? endforeach;
?>
</style>
<div class="stat-wrapper row">
    <?php echo SettingsMenu::widget(); ?>
    <div class="modals-index col-lg-9" id="pjax-modal">

        <h1><?= Html::encode($this->title) ?></h1>
        <!--    <div class="col-sm-4"><button type="button" id="modals-save" class="btn btn-success btn-modals" data-id="--><?//= $model->id ?><!--">Сохранить</button></div>-->
        <input type="hidden" id="modals-save" data-id="<?= $model->id ?>">

        <div class="col-sm-12">
            <?php foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
                echo "<div class=\"helper\"><p class=\"bg-$key\">$message</p></div>";
            } ?>
        </div>

        <ul class="nav nav-tabs modals-nav" role="tablist">
            <li role="presentation" class="active"><a href="#step_0" aria-controls="step_0" role="tab" data-toggle="tab">Шаг 1</a></li>
            <li role="presentation"><a href="#step_1_1" aria-controls="step_1_1" role="tab" data-toggle="tab">Шаг 2 - Да</a></li>
            <li role="presentation"><a href="#step_1_2" aria-controls="step_1_2" role="tab" data-toggle="tab">Шаг 2 - Нет</a></li>
            <li role="presentation"><a href="#step_2_1" aria-controls="step_2_1" role="tab" data-toggle="tab">Шаг 3 - Да</a></li>
            <li role="presentation"><a href="#step_2_2" aria-controls="step_2_2" role="tab" data-toggle="tab">Шаг 3 - Нет</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="step_0">
                <div class="catch-popup">
                    <a href="#" class="btn-popup-close">×</a>
                    <div class="popup-content" id="ref-answer">
                        <span class="js-editable" data-name="text" data-placement="bottom" data-type="textarea" data-title="Текстик"><?= empty($steps['step_0']['text']) ? 'Вы пришли к нам с сайта' : $steps['step_0']['text'] ?></span>
                        <a target="_blank" id="referer" data-name="referer" data-placement="bottom" data-type="text" data-title="Введите адрес" data-id="" href="{$siteurl}" class="js-editable link-from"><?= empty($steps['step_0']['referer']) ? '{$sitename}' : $steps['step_0']['referer'] ?></a>
                        <div class="js-editable" data-name="question"><?= empty($steps['step_0']['question']) ? 'Он вам понравился?' : $steps['step_0']['question'] ?></div>
                        <div class="button-row">
                            <button type="button" class="modal-btn btn-yes js-referer-answer" data-answer="true"><img src="/img/icon-smile.png" alt="" /><span>Да</span></button>
                            <button type="button" class="modal-btn btn-no js-referer-answer" data-answer="false"><img src="/img/icon-sad.png" alt="" /><span>Нет</span></button>
                        </div>
                    </div>
                    <p class="bottom">заменить фон
                        <label class="inner-link js-modal-background" data-target="all" for="file-input">на всех шагах</label>или
                        <label class="inner-link js-modal-background" data-target="this" for="file-input">только на этом</label>
                    </p>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="step_1_1">
                <div class="catch-popup">
                    <a href="#" class="btn-popup-close">×</a>
                    <div class="popup-content" id="ref-yes">
                        <span class="js-editable" data-name="text"><?= empty($steps['step_1_1']['text']) ? 'Спасибо за отзыв, нам это очень важно.' : $steps['step_1_1']['text'] ?></span>
                        <div class="content-big js-editable" data-name="question"><?= empty($steps['step_1_1']['question']) ? 'Хотите сайт? Давайте мы позвоним вам ' : $steps['step_1_1']['question'] ?></div>
                        <form>
                            <input class="modal-input js-comment" type="text" name="comment" placeholder="+ 7 ___ ___-__-__ " required="required" disabled />
                            <button type="button" class="modal-btn js-referer-comment">жду звонка</button>
                            <span>Наберем вас в течение часа</span>
                        </form>
                    </div>
                    <p class="bottom">заменить фон <label class="inner-link js-modal-background" data-target="all" for="file-input">на всех шагах</label> или <label class="inner-link js-modal-background" data-target="this" for="file-input">только на этом</label></p>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="step_1_2">
                <div class="catch-popup">
                    <a href="#" class="btn-popup-close">×</a>
                    <div class="popup-content no-case-content" id="ref-no">
                        <span class="js-editable" data-name="text"><?= empty($steps['step_1_2']['text']) ? 'Спасибо за отзыв, нам это очень важно.' : $steps['step_1_2']['text'] ?></span>
                        <div class="content-big js-editable" data-name="question"><?= empty($steps['step_1_2']['question']) ? 'Расскажете, что&nbsp;не&nbsp;понравилось?' : $steps['step_1_2']['question'] ?></div>
                        <form>
                            <textarea class="js-comment" placeholder="Расскажите, что можно было бы сделать лучше" disabled></textarea>
                            <button type="button" class="modal-btn js-referer-comment">оставить комментарий</button>
                        </form>
                    </div>
                    <p class="bottom">заменить фон <label class="inner-link js-modal-background" data-target="all" for="file-input">на всех шагах</label> или <label class="inner-link js-modal-background" data-target="this" for="file-input">только на этом</label></p>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="step_2_1">
                <div class="catch-popup">
                    <a href="#" class="btn-popup-close">×</a>
                    <div class="popup-content" id="ans-yes">
                        <div class="content-big js-editable" data-name="text"><?= empty($steps['step_2_1']['text']) ? 'Заявка отправлена. Мы&nbsp;позвоним вам, ответим на&nbsp;вопросы и&nbsp;проконсультируем.' : $steps['step_2_1']['text'] ?></div>
                    </div>
                    <p class="bottom">заменить фон <label class="inner-link js-modal-background" data-target="all" for="file-input">на всех шагах</label> или <label class="inner-link js-modal-background" data-target="this" for="file-input">только на этом</label></p>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="step_2_2">
                <div class="catch-popup">
                    <a href="#" class="btn-popup-close">×</a>
                    <div class="popup-content" id="ans-no">
                        <div class="content-big js-editable" data-name="text"><?= empty($steps['step_2_2']['text']) ? 'Комментарий отправлен. Спасибо за&nbsp;мнение!' : $steps['step_2_2']['text'] ?></div>
                    </div>
                    <p class="bottom">заменить фон <label class="inner-link js-modal-background" data-target="all" for="file-input">на всех шагах</label> или <label class="inner-link js-modal-background" data-target="this" for="file-input">только на этом</label></p>
                </div>
            </div>
            <?= Html::fileInput('Upload[file]', null, ['class' => 'modal-file-input', 'accept' => 'image/*', 'id' => 'file-input']) ?>
        </div>
    </div>


</div>