<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Modals */

$this->title = 'Create Modals';
$this->params['breadcrumbs'][] = ['label' => 'Modals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modals-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
