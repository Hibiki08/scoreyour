<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SettingsMenu;

/* @var $this yii\web\View */

$this->title = 'Уведомления и интеграция';
?>
<div class="stat-wrapper row notifications">
    <?php echo SettingsMenu::widget(); ?>
    <div class="domains-index col-lg-9">

        <h1><?= Html::encode($this->title) ?></h1>
        <fieldset>
            <legend>Уведомления</legend>

            <?php foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                echo "<div class=\"helper\"><p class=\"bg-$key\">$message</p></div>";
            } ?>
            <h4>на электронную почту</h4>
            <form action="/notification/set-mail" method="post">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="form-group">
                    <label for="notificationMail">Почтовый адрес</label>
                    <input type="email" class="form-control" name="Users[notification_mail]" id="users-notification_mail"
                           placeholder="Email" value="<?= Yii::$app->user->getIdentity()->getNotificationMail() ?>">
                </div>
                <button type="submit" class="btn btn-default">Сохранить</button>
            </form>

            <h4>в Телеграмме</h4>
            <form action="/notification/set-mail" method="post">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <!--div class="form-group">
                    <label for="notificationMail">Почтовый адрес</label>
                    <input type="email" class="form-control" name="Users[notification_mail]" id="users-notification_mail" placeholder="Email" value="<?= Yii::$app->user->getIdentity()->getNotificationMail() ?>">
                </div-->
                <code>Здесь будет инструкция</code>
<!--                <button type="submit" class="btn btn-default">Сохранить</button>-->
            </form>
        </fieldset>

        <fieldset>
            <legend>Интеграция со сторонними сервисами</legend>
            <h4>Яндекс.Метрика</h4>
            <p>Наш сервис автоматически отправляет в Яндекс.Метрику цели со следующими именами</p>
            <p><span class="target-id">SYW_like</span> — нажатие на кнопку "Да"</p>
            <p><span class="target-id">SYW_dislike</span> — нажатие на кнопку "Нет"</p>
            <p><span class="target-id">SYW_lead</span> — оставили номер телефона</p>
            <p><span class="target-id">SYW_comment</span> — оставили комментарий, что именно не нравится</p>

            <h4>Интеграция с Google Analytics</h4>

            <h4>Интеграция с AMO.CRM</h4>

            <h4>Интеграция с Bitrix24</h4>

            <h4>Интеграция с Roistat</h4>

            <h4>Интеграция с Счетчиком Mail.ru</h4>

            <h4>Интеграция с Yagla</h4>
        </fieldset>
    </div>
</div>

