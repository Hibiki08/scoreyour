<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use bookin\walletone\WalletOne;
use app\components\SettingsMenu;

$this->title = 'Подписка';
?>
<div class="stat-wrapper row">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    Текст
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <?php echo SettingsMenu::widget(); ?>
    <div class="site-subscription col-lg-9">
        <!--    <iframe src="//wl.walletone.com/checkout/form/109026" frameborder="0" allowTransparency height="220" width="350"></iframe>-->
        <h1><?= Html::encode($this->title) ?></h1>
        <p>Дорогие друзья,<br>Стоимость подписки на 1 год — 1999₽</p>
        <?php
        $action = Yii::$app->walletone->apiUrl;
        $formData = Yii::$app->walletone->getFields([
            'WMI_PAYMENT_AMOUNT' => '10',
            'WMI_CUSTOMER_EMAIL' => Yii::$app->user->identity->login,
            'WMI_DESCRIPTION' => '',
            'WMI_PAYMENT_NO' => mt_rand(0,100000),
            'user_id' => Yii::$app->user->id
        ]);
        echo Html::beginForm($action, 'post', ['csrf' => false,  'accept-charset' => 'UTF-8']);
        foreach ($formData as $key => $value) {
            echo Html::hiddenInput($key, $value);
        }
        echo Html::submitButton('Оплатить 1999₽', ['class' => 'btn btn-success']);
        echo Html::endForm();
        echo Html::button('Получить 3 месяца бесплатно', ['class' => 'btn btn-warning free', 'data-toggle' => 'modal', 'data-target' => '#myModal']);
        ?>
    </div>
</div>