<?php
use yii\helpers\Url;
?>
<div id="success" class="signup">
    <?php if (Yii::$app->session->getFlash('confirm-error')) {?>
        <div class="alert alert-danger fade in alert-dismissable">
            <p><?php echo Yii::$app->session->getFlash('confirm-error'); ?></p>
        </div>
    <?php } else {?>
    <div class="alert alert-success fade in alert-dismissable"">
        <p>Почтовый ящик успешно подтверждён!</p>
    </div>
    <?php } ?>
</div>