<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Настройки аккаунта ' . $model->login;
?>

<div class="users-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        echo "<div class=\"helper\"><p class=\"bg-$key\">$message</p></div>";
    } ?>

    <?php $form = ActiveForm::begin(); ?>
    <fieldset>
        <legend>Смена пароля</legend>
        <?= $form->field($model, 'login')->textInput(['maxlength' => true, 'disabled' => true])->label('Логин') ?>
        <?= $form->field($model, 'current_password')->passwordInput(['maxlength' => true])->label('Текущий пароль') ?>
        <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true])->label('Новый пароль') ?>
    </fieldset>

    <fieldset>
        <legend>О компании</legend>
        <?= $form->field($model, 'company')->textInput(['maxlength' => true])->label('Компания') ?>
        <?= $form->field($model, 'domain')->textInput(['maxlength' => true])->label('Домен') ?>
    </fieldset>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
