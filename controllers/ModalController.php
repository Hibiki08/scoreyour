<?php

namespace app\controllers;

use app\models\Upload;
use Yii;
use app\models\Modals;
use app\models\ModalSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;

/**
 * ModalController implements the CRUD actions for Modals model.
 */
class ModalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['userPermission'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Modals models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modal = Modals::find()
            ->where(['user_id' => Yii::$app->user->getId()])
            ->limit(1)
            ->one();
        if (empty($modal)) {
            $modal = new Modals();
            $modal->user_id = Yii::$app->user->getId();
            $modal->save(false);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $modal
        ]);
    }

    /**
     * Displays a single Modals model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Modals model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new Modals();
//        $post = Yii::$app->request->post();
//        $data = [];
//
//        foreach ($post as $key => $fields) {
//            $data["Modals"][$key] = serialize($fields);
//            /*if (is_array($fields)) {
//                foreach ($fields as $field => $value) {
//                    $data["Modals"][$key] = (empty($data["Modals"][$key]) ? '' : ($data["Modals"][$key] . ';')) . "\\\"$field\\\":\\\"" . addslashes($value) . '\"';
//                }
//            }*/
//        }
//
//        if ($model->load($data) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            $model->validate();
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Updates an existing Modals model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            $post = Yii::$app->request->post();
            $data = [];

            foreach ($post as $key => $fields) {
                $data["Modals"][$key] = serialize($fields);
            }

            $model->load($data);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Изменения сохранены.');
                return true;
            }
            return false;
        }

//        return $this->redirect(['index', 'model' => $model]);
    }

    /**
     * Deletes an existing Modals model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    public function actionUploadImage($id) {
        $model = new Upload();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                return $model->upload($id, Yii::$app->request->post('name'));
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Finds the Modals model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Modals the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Modals::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
