<?php

namespace app\controllers;

use app\models\PayHistory;
use app\models\SettingsForm;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\AccessRule;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use app\components\amoCrm;


class SettingsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['userPermission'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $form = new SettingsForm(['scenario' => SettingsForm::SCENARIO_COMPANY]);
        $domainKey = Users::getDomainKey();

        if ($form->load(Yii::$app->request->post())) {

            if ($form->saveCompany()) {
                Yii::$app->session->setFlash('success_company', 'Изменения сохранены!');
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/' . Yii::$app->controller->id]));
            }
        }

        return $this->render('index', [
            'model' => $form,
            'user' => $form->_user,
            'domainKey' => $domainKey
        ]);
    }

    public function actionResetPassword() {
        $form = new SettingsForm(['scenario' => SettingsForm::SCENARIO_RESET_PASS]);

        if ($form->load(Yii::$app->request->post())) {

            if ($form->resetPassword()) {
                Yii::$app->session->setFlash('success_reset', 'Пароль успешно изменён!');
                return Yii::$app->getResponse()->redirect(Url::toRoute([Yii::$app->controller->id . '/reset-password']));
            }
        }

        return $this->render('reset-password', [
            'model' => $form,
            'user' => $form->_user,
        ]);
    }
    
    public function actionPayHistory() {
        $history = PayHistory::getHistory(true);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $history,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        
        return $this->render('pay-history', [
            'history' => $history,
            'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionAmoCrm() {
        $form = new SettingsForm(['scenario' => SettingsForm::SCENARIO_AMOCRM]);
        $amocrmUsers = [];

        $amocrm = new amoCrm(Yii::$app->user->identity->amocrm_domain, Yii::$app->user->identity->amocrm_api_key, Yii::$app->user->identity->amocrm_login);
        if (!empty($amocrm->subdomain) && !empty($amocrm->hash) && !empty($amocrm->login)) {
            if (empty($amocrm->error)) {
                try {
                    $amocrmUsers = $amocrm->getUsers();
                    $amocrmUsers[0] = 'Нет';
                    ksort($amocrmUsers);
                } catch (\AmoCRM\Exception $e) {
                    $this->error = sprintf('Error (%d): %s', $e->getCode(), $e->getMessage());
                }
            }
        }

        if ($form->load(Yii::$app->request->post())) {

            if ($form->saveAmoCrmSettings()) {
                Yii::$app->session->setFlash('success_amocrm', 'Изменения сохранены!');
                return Yii::$app->getResponse()->redirect(Url::current());
            }
        }

        return $this->render('amo-crm', [
            'model' => $form,
            'user' => $form->_user,
            'amocrmUsers' => $amocrmUsers
        ]);
    }

}
