<?php

namespace app\controllers;

use app\models\Domains;
use app\models\Modals;
use app\models\PasswordResetRequestForm;
use app\models\RefererAnswers;
use app\models\ResetPasswordForm;
use app\models\SettingsForm;
use app\models\Users;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\filters\Cors;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use yii\base\ErrorException;
use app\models\PayHistory;
use yii\helpers\Url;
use app\components\amoCrm;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['subscription', 'payment-success', 'payment-fail'],
                        'allow' => true,
                        'roles' => ['userPermission'],
                    ],
                ],
                'only' => ['logout', 'signup', 'login', 'subscription', 'payment-success', 'payment-fail'],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'check-referer' => ['post'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,

                ],

            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['check-referer', 'referer-answer', 'get-catcher', 'payment-check'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signup action.
     *
     * @return Response|string
     */
    public function actionSignup() {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                Yii::$app->session->setFlash('success', 'Для окончания регистрации необходимо подтвердить вашу почту. Письмо с дальнейшими указаниями отправлено вам на почту. <a href="' . Url::to(['site/send-verification', 'email' => $user['login']]) . '">Отправить письмо повторно</a>.');
                return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionConfirmEmail($email, $token) {
        $user = Users::findOne(['login' => $email]);
        if ($user && $user->validateAuthKey($token)) {
            $user->status = Users::STATUS_ACTIVE;
            $user->auth_key = null;
            $user->save();
            Yii::$app->getUser()->login($user);
            Yii::$app->getResponse()->redirect(['/settings']);

        } else {
            Yii::$app->session->setFlash('confirm-error', 'Что-то пошло не так.');
        }
        return $this->render('success');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Displays about page.
     *
     * @return string
     */
//    public function actionAbout()
//    {
//        return $this->render('about');
//    }

    public function actionSendVerification($email) {
        if ($user = Users::findByLogin($email)) {
            Yii::$app->mailer->compose('emailConfirm', ['user' => $user])
                ->setFrom('report@scoreyour.work')
                ->setTo($user->login)
                ->setSubject('Подтверждение регистрации на ' . Yii::$app->name)
                ->send();
            Yii::$app->session->setFlash('success', 'Письмо с дальнейшими указаниями отправлено вам на почту.');
        }
        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'На вашу почту отправлено письмо с дальнейшими указаниями.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем сбросить пароль для указанного почтового адреса.');
            }
        }

        return $this->render('passwordResetRequestForm', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранён.');
            return $this->goHome();
        }

        return $this->render('resetPasswordForm', ['model' => $model]);
    }

    public function actionSubscription() {
        return $this->render('subscription');
    }

    public function actionPaymentCheck() {
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            $walletone = Yii::$app->walletone;

            try{
                if($walletone->checkPayment($post)){
                    $res = 'OK: '. print_r($post,true);
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/payments.log', date("d.m.Y H:i:s") . ' '.$res . "\n", FILE_APPEND);

                    $payHistory = new PayHistory();
                    $payHistory->description = $post['WMI_DESCRIPTION'];
                    $payHistory->pay_status = 1;
                    $payHistory->user_id = $post['user_id'];
                    $payHistory->wallet_data = serialize($post);
                    if ($payHistory->save()) {
                        $user = Users::findOne(['id' => $post['user_id']]);
                        if (!empty($user)) {
                            if (!$user->subscription_status) {
                                $subscriptionDate = new \DateTime(date('Y-m-d'));
                                $user->subscription_date = $subscriptionDate->modify('+1 year')->format('Y-m-d');
                            } else {
                                $subscriptionDate = new \DateTime($user->subscription_date);
                                $user->subscription_date = $subscriptionDate->modify('+1 year')->format('Y-m-d');
                            }
                            $user->subscription_status = 1;
                            $user->update();

                        }

                    }
                }
            }
            catch (ErrorException $c){
                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/payments.log', date("d.m.Y H:i:s"). ' '.$c->getMessage() . "\n", FILE_APPEND);
                $payHistory = new PayHistory();
                $payHistory->description = $post['WMI_DESCRIPTION'];
                $payHistory->user_id = $post['user_id'];
                $payHistory->wallet_data = serialize($post);
                $payHistory->save();
                return 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.$c->getMessage();
            }
            return 'WMI_RESULT=OK';
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/payments.log', date("d.m.Y H:i:s"). "OK\n", FILE_APPEND);
        }

    }

    public function actionPaymentSuccess() {
        return $this->render('payment-success');
    }

    public function actionPaymentFail() {
        return $this->render('payment-fail');
    }

    public function actionCheckReferer()
    {
        header("Access-Control-Allow-Origin: *");
        $referer = Yii::$app->request->post('referer');
        $host = Yii::$app->request->post('host');
        $url = Yii::$app->request->post('url');

        if (empty($referer) || empty($host) || empty($url)) {
            return null;
        }

        $user = Users::find()
            ->select('id')
            ->where(['like', 'domain', $host])
            ->one();

        if (!empty($user)) {
            $domain = Domains::find()
                ->where(['user_id' => $user['id']])
                ->andWhere(['like', 'host', $referer])
                ->asArray()
                ->one();

            return json_encode($domain);
        }

        return null;
    }

    public function actionRefererAnswer() {
        header("Access-Control-Allow-Origin: *");
        $id = Yii::$app->request->post('id');

        if (empty($id)) {
            $model = new RefererAnswers();
            $model->load(Yii::$app->request->post());
            $model->ip = empty($_SERVER['REMOTE_ADDR']) ? null : $_SERVER['REMOTE_ADDR'];

            if ($model->save()) {
                return $model->id;
            }
        } else {
            $model = RefererAnswers::find()
                ->where(['id' => $id])
                ->one();

            if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
                $domain = Domains::findOne(['id' => $model->domain_id]);
                $user = Users::findOne(['id' => $domain->user_id]);

                try {
                    $amocrm = new amoCrm($user['amocrm_domain'], $user['amocrm_api_key'], $user['amocrm_login']);
                    $amo = $amocrm->getClient();

                    if (!empty($user['amocrm_responsible_id'])) {
                        $lead = $amo->lead;
                        $lead['name'] = $domain->host;
                        $lead['status_id'] = $amo->fields->StatusId;
                        $lead['responsible_user_id'] = $user['amocrm_responsible_id'];
                        $lead['tags'] = ['ScoreYourWork'];
                        if (preg_match('/^\+7\(\d{3}\)\d{3}-\d{2}-\d{2}$/', $model->comment)) {
                            $lead->addCustomField(941671, $model->comment);
                        } else {
                            $lead->addCustomField(941673, $model->comment);
                        }

                        $lead->apiAdd();
                    }
                } catch (\AmoCRM\Exception $e) {
                    echo 'Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage();
                }

                Yii::$app->mailer->compose('answerNotification', ['answer' => $model, 'domain' => $domain])
                    ->setFrom('report@scoreyour.work')
                    ->setTo($user->notification_mail)
                    ->setSubject('Новый комментарий на ScoreYour.Work')
                    ->send();
            }
        }
    }

//    public function actionSendReport () {
//        $users = Users::find()
//            ->where(['status' => 1])
//            ->all();
//
//        foreach ($users as $user) {
//            $dataProvider = new ActiveDataProvider([
//                'query' => (new Query())->from('referer_answers')
//                    ->select('host, SUM(answer=1) AS "yes", COUNT(*) AS "all"')
//                    ->innerJoin('domains', 'domains.id = domain_id')
//                    ->where(['domains.user_id' => $user->id])
//                    ->andWhere(new Expression('UNIX_TIMESTAMP(date) > ' . strtotime('-7 day')))
//                    ->groupBy('domain_id'),
//            ]);
//
//            Yii::$app->mailer->compose('weeklyReport', ['dataProvider' => $dataProvider])
//                ->setFrom('inbox@activemedia.pro')
//                ->setTo($user->notification_mail)
//                ->setSubject('Новый комментарий на ScoreYour.Work')
//                ->send();
//        }
//    }

    public function actionGetCatcher () {
        header("Access-Control-Allow-Origin: *");
        $domain_key = Yii::$app->request->get('domain_key');
        $user = Users::find()
            ->select('id')
            ->where(['domain_key' => $domain_key, 'subscription_status' => 1])
            ->one();

        $steps = [];
        if (!empty($user)) {
            $modal = Modals::find()
                ->where(['user_id' => $user['id']])
                ->asArray()
                ->one();

            foreach ($modal as $key => $value) {
                $value = @unserialize($value);
                if (!empty($value) && $value !== false) {
                    $steps[$key] = $value;
                }
            }

            $result = $this->renderPartial('catcher', [
                'steps' => $steps
            ]);

            $result = preg_replace('/{\$sitename}/', 'data[\'name\']', $result);

            return $result;
        }

        return false;
    }
}
