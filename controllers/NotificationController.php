<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class NotificationController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['userPermission'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionSetMail() {
        $user = Users::findOne(Yii::$app->user->getId());

        if ($user && $user->load(Yii::$app->request->post()) && $user->save(false)) {
            Yii::$app->session->setFlash('success', 'Изменения сохранены.');
        }
        return $this->redirect(['index']);
    }

}
