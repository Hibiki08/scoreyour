<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use app\models\RefererAnswers;
use app\models\RefererAnswerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Domains;

/**
 * RefererAnswerController implements the CRUD actions for RefererAnswers model.
 */
class StatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefererAnswers models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RefererAnswerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $urls = Domains::getDomainsByUser();
        $urlArr = [];
        if (!empty($urls)) {
            foreach ($urls as $val) {
                $urlArr[$val['id']] = $val['host'];
            }
        }

        $topReferer = (new Query())->from('referer_answers')
            ->select('COUNT(*) AS "all", host')
            ->innerJoin('domains', 'domains.id = domain_id')
            ->where(['domains.user_id' => Yii::$app->user->id])
            ->orderBy(['all' => SORT_DESC])
            ->groupBy('domain_id')
            ->one();

        $successReferer = (new Query())->from('referer_answers')
            ->select('SUM(answer=1)/COUNT(*) * 100 AS "stat", host')
            ->innerJoin('domains', 'domains.id = domain_id')
            ->where(['domains.user_id' => Yii::$app->user->id])
            ->orderBy(['stat' => SORT_DESC])
            ->groupBy('domain_id')
            ->one();

        $failReferer = (new Query())->from('referer_answers')
            ->select('SUM(answer=1)/COUNT(*) * 100 AS "stat", host')
            ->innerJoin('domains', 'domains.id = domain_id')
            ->where(['domains.user_id' => Yii::$app->user->id])
            ->orderBy(['stat' => SORT_ASC])
            ->groupBy('domain_id')
            ->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'topReferer' => $topReferer,
            'successReferer' => $successReferer,
            'failReferer' => $failReferer,
            'urls' => $urlArr
        ]);
    }

    public function actionDomains() {
        $statisticsDP = new ActiveDataProvider([
            'query' => (new Query())->from('referer_answers')
                ->select('host, SUM(answer=1) AS "yes", COUNT(*) AS "all"')
                ->innerJoin('domains', 'domains.id = domain_id')
                ->where(['domains.user_id' => Yii::$app->user->id])
                ->groupBy('domain_id'),
        ]);
        
        return $this->render('domains', [
            'statisticsDP' => $statisticsDP,
        ]);
    }

    /**
     * Displays a single RefererAnswers model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new RefererAnswers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new RefererAnswers();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Updates an existing RefererAnswers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing RefererAnswers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the RefererAnswers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefererAnswers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefererAnswers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
